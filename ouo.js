import {chromium} from 'playwright-chromium'

const browser = await chromium.launch({executablePath:'/usr/bin/google-chrome', args:['--disable-gpu'], proxy:{server:'socks5://181.6.155.38:1080'}})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
const context = await browser.newContext()
await context.addCookies([
  {
    "name": "playlistpos_5553",
    "value": "723037",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1629057181,
    "httpOnly": false,
    "secure": true,
    "sameSite": "Strict"
  },
  {
    "name": "_pubcid",
    "value": "d1cdaed8-af04-4cf0-85bd-3a0bf3160036",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1642017160,
    "httpOnly": false,
    "secure": false,
    "sameSite": "Lax"
  },
  {
    "name": "PHPSESSID",
    "value": "biceu29edbdhc1qcrl6hdtl1v1",
    "domain": "hideout.co",
    "path": "/",
    "expires": -1,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_ga",
    "value": "GA1.2.1785421646.1626465161",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1689537796,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_gid",
    "value": "GA1.2.1073123428.1626465161",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1626552196,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "__qca",
    "value": "P0-98054740-1626465161532",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1660333961,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "usprivacy",
    "value": "1YNN",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1660161163.638279,
    "httpOnly": false,
    "secure": true,
    "sameSite": "Lax"
  },
  {
    "name": "fromLeak",
    "value": "0",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1629057369,
    "httpOnly": false,
    "secure": true,
    "sameSite": "Strict"
  },
  {
    "name": "_lr_geo_location",
    "value": "US",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1626551564.787501,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "OB-USER-TOKEN",
    "value": "c84287d1-7fa1-499e-a39a-f749dd51ccf7",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1634241164.813299,
    "httpOnly": false,
    "secure": false,
    "sameSite": "Lax"
  },
  {
    "name": "__gads",
    "value": "ID=df63ccd98c6cd870:T=1626465166:S=ALNI_MbtxRHsjdsGtmvnkROu5TJok3CWUQ",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1660161166,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_lr_retry_request",
    "value": "true",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1626468768,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "pbjs-unifiedid",
    "value": "%7B%22TDID%22%3A%2233d1fa2c-ab45-4bf8-a4be-49c567dc5701%22%2C%22TDID_LOOKUP%22%3A%22TRUE%22%2C%22TDID_CREATED_AT%22%3A%222021-06-16T19%3A52%3A48%22%7D",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1631649168,
    "httpOnly": false,
    "secure": false,
    "sameSite": "Lax"
  },
  {
    "name": "cto_bidid",
    "value": "5Hm7-183V2tOUTZ4eldodjllZFpKUFZtc3pwQno2M3UlMkJCUTM1R3BYY0U4MUtqY2lyMXVBN1B3RUklMkJhMUlDRnpCJTJGbE9OaEZiakpzdERvbnJ4diUyRm9vVjlnVzF3JTNEJTNE",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1660143168,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "cto_bundle",
    "value": "HXbL4l82QTlMZGNjdGR2ZVhjSGVTUThQQ0g3Qld6OXM0TTRyRlZlT3JuWWdOdldXODNEdzNYeDR0QnM5UkRwcnJZenklMkZ5M0ZzS00lMkZrMm4xTkNHYXdPJTJGSUpxbkJqWkE0RWVsTzA2VTlTOEx5Um52NGpOeUJYekNRRXBWQjZTbiUyQkdmcThY",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1660143168,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "cnx_userId",
    "value": "5b974da478014413a1df8901ccf35df5",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1629057797,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_pbjs_userid_consent_data",
    "value": "6683316680106290",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1629057369,
    "httpOnly": false,
    "secure": false,
    "sameSite": "Lax"
  },
  {
    "name": "G_ENABLED_IDPS",
    "value": "google",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 253402257600,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_lr_env",
    "value": "eyJlbnZlbG9wZSI6IkFpTjRDT3Bzcm9iN1FFeXplM1JzUWhGWFc3TXU0empZZVJ0T1RaR1pncjl0eEFTVWZIVXBJT29ia0owQ3B6U1A4eVZEUk43VjNlVmpiRm9weHJUSTZIcDlGMDJlQ1Y1WjNZZ1dsa1FQbi1rT1JHdjJhT2dYVHJiLVBZdE1RZmN4ZS14NCIsInRpbWVzdGFtcCI6MTYyNjQ2NTM2OTM0NSwidmVyc2lvbiI6IjEuNi4xIn0%3D",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1658001369.346473,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "_lr_env_src_ats",
    "value": "true",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1629057373,
    "httpOnly": false,
    "secure": false
  },
  {
    "name": "idl_env",
    "value": "AiN4COpsrob7QEyze3RsQhFXW7Mu4zjYeRtOTZGZgr9txASUfHUpIOobkJ0CpzSP8yVDRN7V3eVjbFopxrTI6Hp9F02eCV5Z3YgWlkQPn-kORGv2aOgXTrb-PYtMQfcxe-x4",
    "domain": "hideout.co",
    "path": "/",
    "expires": 1627070173,
    "httpOnly": false,
    "secure": false,
    "sameSite": "Lax"
  },
  {
    "name": "_gat_gtag_UA_8269309_12",
    "value": "1",
    "domain": ".hideout.co",
    "path": "/",
    "expires": 1626465851,
    "httpOnly": false,
    "secure": false
  }
])
const alexamaster = await context.newPage()
await alexamaster.goto('https://hideout.co', {timeout:0})
await alexamaster.click('div.videoItem>a')
globalThis.setInterval(async () => console.log(await alexamaster.innerHTML('div.dropdown')), 5 * 60 * 1000)
